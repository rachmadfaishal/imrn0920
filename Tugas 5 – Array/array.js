// No. 1 (Range).
function range(num1,num2){
	let array=[];
	if(num1<=num2){
		for(i=num1; i<=num2; i++){
			array.push(i);
		} 
    } 
    if(num1>=num2){
		for(i=num1; i>=num2; i--){
			array.push(i);
		} 
    } 
    else if (!num1 || !num2){
        return -1
    }
	return array;
}
console.log(range(1, 10))
console.log(range(1))
console.log(range(11,18))
console.log(range(54, 50))
console.log(range())
console.log('\n');

// No.2 (Range with Step).
function rangeWithStep(num1,num2,step){
	let array=[];
	if(num1<=num2){
		for(i=num1; i<=num2; i = i+step){
			array.push(i);
		} 
    } 
    if(num1>=num2){
		for(i=num1; i>=num2; i = i-step){
			array.push(i);
		} 
    } 

	return array;
}
console.log(rangeWithStep(1, 10, 2));
console.log(rangeWithStep(11, 23, 3));
console.log(rangeWithStep(5, 2, 1));
console.log(rangeWithStep(29, 2, 4));
console.log('\n');

// No.3 (Sum of Range).
function sum (num1, num2, step){
    var rangeArr = [];
    var distance;
    
    if (!step){
        distance=1
    } else{
        distance=step
    }
    
    if(num1>num2){
        var currentnum=num1;
		for(var i=0; currentnum>=num2; i++){
            rangeArr.push(currentnum)
            currentnum -= distance
		} 
    } 
	else if(num1<num2){
		var currentnum=num1;
		for(var i=0; currentnum<=num2; i++){
            rangeArr.push(currentnum)
            currentnum += distance
		} 
    } 
    else if (!num1 && !num2 && !step){
        return 0
    }
    else if (num1){
        return num1
    }
    var total =0;
	for(var i=0; i< rangeArr.length; i++){
			total= total + rangeArr[i];
	} 
	return total;
}
console.log(sum(1,10));
console.log(sum(5, 50, 2));
console.log(sum(15,10));
console.log(sum(20, 10, 2));
console.log(sum(1));
console.log(sum());
console.log('\n');

// No.4 (Array Multidimensi).
var input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
]

function dataHandling(input){
	for(var i = 0; i < input.length; i++) {
    for (var j = 0; j < input[i].length; j++) {
      var value = input[i][j]
      var text = "";
      switch(j) {
        case 0:
          text = "Nomor ID: " + value
          break
        case 1:
          text = "Nama Lengkap: " + value
          break
        case 2:
          var value2 = input[i][3]
          text = "TTL: " + value + " " + value2
          break
        case 3:
          break
        case 4:
          text = "Hobi: " + value +  '\n'
          break
      }
      if (j !== 3) {
        console.log(text)
      }
    }
  }
}
dataHandling(input);
console.log('\n');

// No.5 (Balik Kata).
function balikKata(Kata) {
    var kataBaru = '';
       for (let i = Kata.length - 1; i >= 0; i--) {
           kataBaru = kataBaru + Kata[i];
       }
    return kataBaru;
}
console.log(balikKata("Kasur Rusak"));
console.log(balikKata("SanberCode"));
console.log(balikKata("Haji Ijah"));
console.log(balikKata("racecar"));
console.log(balikKata("I am Sanbers"));
console.log('\n');

// No.6 (Metode Array).
var input = ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"]  
function dataHandling2(input) {
    input.splice(1, input.length - 1,  "Roman Alamsyah Elsharawy", "Provinsi Bandar Lampung", "21/05/1989", "Pria", "SMA Internasional Metro")
    console.log(input)
 
    var date = input[3]
    date = date.split('/')
     var month = date[1]
     var monthText = ''
    switch(month) {
      case '01':
        monthText = "Januari"
        break;
      case "02":
        monthText = "Februari"
        break;
      case "03":
        monthText = "Maret"
        break;
      case "04":
        monthText = "April"
        break;
      case "05":
        monthText = "Mei"
        break;
      case "06":
        monthText = "Juni"
        break;
      case "07":
        monthText = "Juli"
        break;
      case "08":
        monthText = "Agustus"
        break;
      case "09":
        monthText = "September"
        break;
      case "10":
        monthText = "Oktober"
        break;
      case "11":
        monthText = "November"
        break;
      case "12":
        monthText = "Desember"
        break;
      default:
        monthText = "Data tidak sesuai"
        break;
    }
    console.log(monthText)
    
    var sortedDate = []
    for(var x = 0; x < date.length; x++) {
      sortedDate.push(date[x])
    }
    sortedDate = sortedDate.sort(function(a, b){return b-a})
    console.log(sortedDate)
 
    var strippedDate = []
    for(var x = 0; x < date.length; x++) {
      strippedDate.push(date[x])
    }
    strippedDate = strippedDate.join("-")
    console.log(strippedDate)
    var name = input[1]
    name = name.slice(0, 14)

    console.log(name)
    }
    
    dataHandling2(input);
    console.log('\n');
    