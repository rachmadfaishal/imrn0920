// No. 1 (Array to Object).
console.log( "========== SOAL NO.1 - Array to Object ==========" + '\n');

var now = new Date()
var thisYear = now.getFullYear() // 2020 (tahun sekarang)

function arrayToObject(arr){
    if (arr.length > 0) {
    for(var i = 0; i < arr.length; i++) {
      var peopleObj = {
          firstName : arr[i][0],
          lastName : arr[i][1],
          gender : arr[i][2],
          age : thisYear - arr[i][3], 
      }
     if (people[i][3] === undefined || peopleObj.age <= 0){
      peopleObj.age = 'Invalid Birth Year'
      }
    console.log(i+1 +'. ' + peopleObj.firstName+' '+peopleObj.lastName+' :')
    console.log(peopleObj)
    console.log('\n');
    } 
  }else {
    console.log('""');
  }
}
var people = [ ["Bruce", "Banner", "male", 1975], 
                ["Natasha", "Romanoff", "female"] ]
arrayToObject(people);

var people2 = [ ["Tony", "Stark", "male", 1980], 
                ["Pepper", "Pots", "female", 2023] ]
arrayToObject(people2);

arrayToObject([]);

console.log('\n');
//===============================================================================================

// No.2 (Shopping Time).
console.log( "========== SOAL NO.2 - Shopping Time ==========" + '\n');

function shoppingTime(memberId, money) {
  var shopping = {}
  var item = [
                  ['Sepatu Stacattu', 1500000],
                  ['Baju Zoro', 500000],
                  ['Baju H&N', 250000],
                  ['Sweater Uniklooh', 175000],
                  ['Casing Handphone', 50000]
               ]

  if(memberId === ''){
    return "Mohon maaf, toko X hanya berlaku untuk member saja"
  } else {
    shopping.memberId = memberId
  }

  if(money <= 50000 ){
    return 'Mohon maaf, uang tidak cukup'
  } else {
    shopping.money = money
  }

  if(memberId === undefined && money === undefined){
    return 'Mohon maaf, toko X hanya berlaku untuk member saja'
  }

  var itemPrice = 0
  shopping.listPurchased = []
  for(let i = 0; i < item.length; i++){
    
    if(money > item[i][1]){
      shopping.listPurchased.push(item[i][0])
      itemPrice += item[i][1]
    }
    shopping.changeMoney = money - itemPrice
  }
   return shopping
}

console.log(shoppingTime('1820RzKrnWn08', 2475000));
console.log(shoppingTime('82Ku8Ma742', 170000));
console.log(shoppingTime('', 2475000));
console.log(shoppingTime('234JdhweRxa53', 15000));
console.log(shoppingTime());

console.log('\n');
//================================================================================================

// No. 3 (Naik Angkot).
console.log( "========== SOAL NO.3 - Naik Angkot ==========" + '\n');

function naikAngkot(arrPenumpang){
  rute = ['A', 'B', 'C', 'D', 'E', 'F'];
  
  var angkot = [{},{}];
  var asal = '';
  var tujuan = '';
  if (arrPenumpang.length > 0) {
  for (var i=0; i<arrPenumpang.length; i++) {
      for (var j=0; j<arrPenumpang[i].length; j++) {
          switch (j) {
              case 0: {
                  angkot[i].penumpang = arrPenumpang[i][j];
                  break;
              } case 1: {
                  angkot[i].naikDari = arrPenumpang[i][j];
                  break;
              } case 2: {
                  angkot[i].tujuan = arrPenumpang[i][j];
                  break;
              }
          }
          asal = angkot[i].naikDari;
          tujuan = angkot[i].tujuan;
          var jarak = 0;
          for (var k=0; k<rute.length; k++) {
              if (rute[k] === asal) {
                  for (var l=k+1; l<rute.length; l++) {
                      jarak += 1;
                      if (rute[l] === tujuan) {
                          var bayar = jarak * 2000;
                          angkot[i].bayar = bayar;
                      }
                  }
              }
          }
      }
  }
    return angkot;
  } else {
    return angkot = '[]';
  }
}
console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]));
console.log(naikAngkot([]));

console.log('\n');