// Soal No.1 Membuat Kalimat.
var word = 'JavaScript '; 
var second = 'is '; 
var third = 'awesome '; 
var fourth = 'and '; 
var fifth = 'I '; 
var sixth = 'love '; 
var seventh = 'it!';

console.log(word.concat(second, third, fourth, fifth, sixth, seventh));
console.log();

// Soal No.2 Mengurai kalimat.
var sentence = "I am going to be React Native Developer"; 

var exampleFirstWord = sentence[0] ; 
var exampleSecondWord = sentence[2] + sentence[3] ; 
var thirdWord = sentence.substr(5, 5);
var fourthWord = sentence.substr(11, 2); 
var fifthWord = sentence.substr(14, 2); 
var sixthWord = sentence.substr(17, 5);
var seventhWord = sentence.substr(23, 6); 
var eighthWord = sentence.substr(30, 9);

console.log('First Word: ' +exampleFirstWord); 
console.log('Second Word: ' +exampleSecondWord); 
console.log('Third Word: ' +thirdWord); 
console.log('Fourth Word: ' +fourthWord); 
console.log('Fifth Word: ' +fifthWord); 
console.log('Sixth Word: ' +sixthWord); 
console.log('Seventh Word: ' +seventhWord); 
console.log('Eighth Word: ' +eighthWord)
console.log();

// Soal No. 3 Mengurai Kalimat.
var sentence2 = 'wow JavaScript is so cool'; 

var exampleFirstWord2 = sentence2.substring(0, 3); 
var secondWord2 = sentence2.substring(4, 14); 
var thirdWord2 = sentence2.substring(15, 17);  
var fourthWord2 = sentence2.substring(18, 20); 
var fifthWord2 = sentence2.substring(21, 25);  

console.log('First Word: ' +exampleFirstWord2); 
console.log('Second Word: ' +secondWord2); 
console.log('Third Word: ' +thirdWord2); 
console.log('Fourth Word: ' +fourthWord2); 
console.log('Fifth Word: ' +fifthWord2);
console.log();

// Soal No. 4 Mengurai Kalimat dan Menentukan Panjang String.
var sentence3 = 'wow JavaScript is so cool'; 

var exampleFirstWord3 = sentence3.substring(0, 3); 
var secondWord3 = sentence3.substring(4, 14); // do your own! 
var thirdWord3 = sentence3.substring(15, 17); // do your own! 
var fourthWord3 = sentence3.substring(18, 20); // do your own! 
var fifthWord3 = sentence3.substring(21, 25); // do your own! 

var firstWordLength = exampleFirstWord3.length
var secondWord3Length = secondWord3.length
var thirdWord3Length = thirdWord3.length
var fourthWord3Length = fourthWord3.length
var fifthWord3Length = fifthWord3.length

console.log('First Word: ' +exampleFirstWord3 + ', with length: ' +firstWordLength); 
console.log('Second Word: ' +secondWord3 + ', with length: ' +secondWord3Length); 
console.log('Third Word: ' +thirdWord3 + ', with length: ' +thirdWord3Length); 
console.log('Fourth Word: ' +fourthWord3 + ', with length: ' +fourthWord3Length); 
console.log('Fifth Word: ' +fifthWord3 + ', with length: ' +fifthWord3Length); 
console.log();
