// Soal If-else
var nama="Jane";
var peran= "Penyihir";

if (nama == "John" ) {
    
    if (peran == "" )
    console.log("Halo John, Pilih peranmu untuk memulai game!");
  } else if (nama == "Jane") {
    console.log("Selamat datang di Dunia Werewolf, Jane");
    if (peran == "Penyihir" )
    console.log("Halo Penyihir Jane, kamu dapat melihat siapa yang menjadi werewolf!");
  } else if (nama == "Jenita") {
    console.log("Selamat datang di Dunia Werewolf, Jenita");
    if (peran == "Guard" )
    console.log("Halo Guard Jenita, kamu akan membantu melindungi temanmu dari serangan werewolf.");
  } else if (nama == "Junaedi") {
    console.log("Selamat datang di Dunia Werewolf, Junaedi");
    if (peran == "Werewolf" )
    console.log("Halo Werewolf Junaedi, Kamu akan memakan mangsa setiap malam!");
  } else {
    console.log("Nama harus diisi!");
  }

console.log();
//---------------------------------------------------
// Soal Switch Case

var hari = 29
var bulan = 9
var tahun = 2020

let bulanText = ""
switch (bulan) {
  case 1:
    bulanText = "Januari"
    break;
  case 2:
    bulanText = "Februari"
    break;
  case 3:
    bulanText = "Maret"
    break;
  case 4:
    bulanText = "April"
    break;
  case 5:
    bulanText = "Mei"
    break;
  case 6:
    bulanText = "Juni"
    break;
  case 7:
    bulanText = "Juli"
    break;
  case 8:
    bulanText = "Agustus"
    break;
  case 9:
    bulanText = "September"
    break;
  case 10:
    bulanText = "Oktober"
    break;
  case 11:
    bulanText = "November"
    break;
  case 12:
    bulanText = "Desember"
    break;
  default:
    break;
}

if (hari < 1 || hari > 31) {
  console.log("Input tanggal tidak sesuai")
} else if (bulan < 1 || bulan > 12) {
  console.log("Input bulan tidak sesuai")
} else {
  console.log(hari + " " + bulanText + " " + tahun)
}
console.log();
//---------------------------------------------------