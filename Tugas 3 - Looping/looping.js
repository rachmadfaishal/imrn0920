// No. 1 Looping While
console.log('LOOPING PERTAMA');
var deret=0;
while(deret < 20){
    deret += 2;
    console.log(deret + ' - I Love Coding');
}
console.log('LOOPING KEDUA');
var counter=22;
while(counter > 2){
    counter -= 2;
    console.log(counter + ' - I will become a mobile developer');
}

console.log("\n\n")

// No. 2 Looping For

for( var nilai=1; nilai <= 20;){
    if(nilai%2==0)
    {console.log(nilai+" - Berkualitas" );
}    
else {
    if(nilai%3==0)
    {   
    console.log(nilai+" - I Love Coding" );
    } else {console.log(nilai+" - Santai" );}
}
nilai++;
}

console.log("\n\n")

// No. 3 Membuat Persegi Panjang #.
for (var y=1;y<=4;y++){
    for (var x=1;x<=8;x++){
        process.stdout.write("#");
    }
    process.stdout.write("\n");
}

console.log("\n\n")

// No. 4 Membuat Tangga.
for (var y=1;y<=7;y++){
    for (var x=1;x<=y;x++){
        process.stdout.write("#");
    }
    process.stdout.write("\n");
}

console.log("\n\n")

// No. 5 Membuat Papan Catur.
for (var y=1;y<9;y++){
    for (var x=1;x<6;x++){
      if( y%2 !== 0) {
        if (x === 1) {
          process.stdout.write(" ");
        } else {
          process.stdout.write("# ");

        }
      } else {
        if (x === 5) {
          process.stdout.write(" ");
        } else {
          process.stdout.write("# ");
        }
      }
    }
    process.stdout.write("\n");
}

console.log("\n\n")